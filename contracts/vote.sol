// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";

contract ContratDeVote is Ownable {
    enum EtatDuVote {
        EnregistrementDesVotants,
        DebutEnregistrementPropositions,
        FinEnregistrementPropositions,
        DebutSessionDeVote,
        FinSessionDeVote,
        VotesComptabilises
    }

    struct Votant {
        bool estInscrit;
        bool aVote;
        uint idPropositionVotee;
    }

    struct Proposition {
        string description;
        uint nombreDeVotes;
    }

    EtatDuVote public etatDuVote;
    uint public idPropositionGagnante;
    uint public nombreDeVotants;
    uint public quorumRequis;

    mapping(address => Votant) public votants;
    Proposition[] public propositions;

    constructor() {
        etatDuVote = EtatDuVote.EnregistrementDesVotants;
    }

    modifier seulementA(EtatDuVote _etat) {
        require(etatDuVote == _etat, "Etat du vote invalide");
        _;
    }

    function enregistrerVotant(
        address _adresseVotant
    ) external onlyOwner seulementA(EtatDuVote.EnregistrementDesVotants) {
        require(!votants[_adresseVotant].estInscrit, "Votant deja inscrit");
        votants[_adresseVotant].estInscrit = true;
        nombreDeVotants++;
        emit VotantInscrit(_adresseVotant);
    }

    // ... (les autres fonctions restent inchangées)

    event VotantInscrit(address adresseVotant);
    event ChangementEtatDuVote(EtatDuVote etatPrecedent, EtatDuVote nouvelEtat);
    event PropositionEnregistree(uint idProposition);
    event Vote(address votant, uint idProposition);
    event VoteRevoque(address votant, uint idProposition);
}
